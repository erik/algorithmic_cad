import cadquery as cq
import math

# input measurements in mm
stud_diameter = 6.25
stud_length = 10.0
rod_diameter = 19.05
ledge_angle = 180 * 2.0 * math.pi / 360.0
ledge_length = 15.0
plate_thickness = 5.0
wall_thickness = 3.5

# derived values
stud_radius = 0.5 * stud_diameter
rod_radius = 0.5 * rod_diameter
base_radius = rod_radius + wall_thickness

# these four points define the rod ledge
angle_left = 0.75 * 2.0 * math.pi - 0.5 * ledge_angle
angle_right = 0.75 * 2.0 * math.pi + 0.5 * ledge_angle
p1 = ((rod_radius + wall_thickness) * math.cos(angle_left),
      (rod_radius + wall_thickness) * math.sin(angle_left))
p2 = (rod_radius * math.cos(angle_left), rod_radius * math.sin(angle_left))
p3 = (rod_radius * math.cos(angle_right), rod_radius * math.sin(angle_right))
p4 = ((rod_radius + wall_thickness) * math.cos(angle_right),
      (rod_radius + wall_thickness) * math.sin(angle_right))

result = (
    cq.Workplane("YZ")
    .cylinder(stud_length, stud_radius)
    .faces(">X")
    .workplane(origin=(0.0, 0.0, stud_radius - base_radius))
    .cylinder(plate_thickness, base_radius)
    .faces(">X")
    .workplane()
    .moveTo(*p1)
    .lineTo(*p2)
    .threePointArc((0.0, -rod_radius), p3)
    .lineTo(*p4)
    .threePointArc((0.0, -(rod_radius + wall_thickness)), p1)
    .close()
    .extrude(ledge_length)
)

cq.exporters.export(result, "rod_hanger.stl")
