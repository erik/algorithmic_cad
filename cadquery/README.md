# CADQuery

![CADQuery GUI](img/cqgui.png)

## Quick Links

- [docs](https://cadquery.readthedocs.io/en/latest/)
- [examples](https://cadquery.readthedocs.io/en/latest/examples.html)
- [main repo](https://github.com/CadQuery/cadquery)
- [GUI repo](https://github.com/CadQuery/CQ-editor)

## About

CADQuery is a Python library for doing CAD with code. In this regard it follows in the footsteps of
[OpenSCAD](https://openscad.org/). However CADQuery has a higher level API that allows you to write
scripts that look more like reasonable verbal descriptions of the part (or at least what you'd do to
make it in a standard CAD program). In particular, it's easy to define new workplanes, make 2D
sketches in them, and extrude, as one would in standard CAD. CADQuery also has support for common
operations like filleting, chamfers, and hollowing (making shells). And it supports constraints!

CADQuery started as a wrapper around the [FreeCAD](https://www.freecadweb.org/) API. But now it's
built on top of the underlying [Open CASCADE
kernel](https://www.opencascade.com/open-cascade-technology/) directly so that it can support some
more advanced geometric selection and editing.

Most importantly, CADQuery functions as a Python library, so you can write Python scripts that
generate geometry and export it (as STL, STEP, etc.). There's also a dedicated GUI that shows your
code in one pane, and the constructed part in another (along with a console and some debugging
info).

## Installation

### Standalone

The instructions here are based off of [this video](https://www.youtube.com/watch?v=sjLTePOq8bQ).

The standalone CADQuery library is installed via Anaconda or Miniconda. Miniconda requires far less
disk space, so it is recommended if you don't already have Anaconda installed. To install Miniconda,
download the [installation script](https://docs.conda.io/en/latest/miniconda.html) for your
operating system. Hashes are provided so you can ensure that your download is correct. By default,
Miniconda will install locally (i.e. in your home directory). For me, this is `~/miniconda3`.

Once Miniconda is installed, we want to make a conda environment in which we can install CADQuery.
First we need to activate Miniconda: `source ~/miniconda3/bin/activate`. Your terminal prompt should
now include `(base)` at the beginning of the line. Next we create a conda environment for CADQuery:
`conda create -n cadquery`. Once that is created, we can activate that environment with `conda
activate cadquery`. Now your prompt should say `(cadquery)` at the beginning.

Finally, we install CADQuery: `conda install -c conda-forge -c cadquery cadquery=master`. CADQuery
has quite a few dependencies, so it's normal that those will be installed at this time as well. Note
that the last option (`cadquery=master`) ensures we get the latest development release. If you want
to install the latest stable release, leave it off. But CADQuery is under active development, so you
may have better luck reading the docs and following examples with the latest release.

To test that the install is functional, start a Python
[REPL](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) (i.e. just run `python`),
then import CADQuery: `import cadquery`. If there's no error, it's working. Now you can continue
running the test commands below, or just jump into some of the examples in this repo or on the
[CADQuery website](https://cadquery.readthedocs.io/en/latest/examples.html). (When you're done can
exit the Python shell by running `exit()`.)

```
(cadquery) user@computer:~/algorithmic_cad$ python
Python 3.10.2 | packaged by conda-forge | (main, Feb  1 2022, 19:28:35) [GCC 9.4.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import cadquery
>>> result = cadquery.Workplane("XY").box(1, 1, 1)
>>> print(result)
<cadquery.cq.Workplane object at 0x7fd6a24b21d0>
```

### GUI

There are several ways to install the CADQuery GUI. Their website recommends using an installer
script, but I would recommend using Miniconda as we did for the standalone version.

#### Miniconda

First, clone the [CADQuery Editor repo](https://github.com/CadQuery/CQ-editor). From within that
project, run `conda env create -f cqgui_env.yml -n cqgui`, followed by `conda activate cqgui`. You
can then start the GUI by running `python run.py`.

Note: In theory, you can also install the GUI without cloning any repos by making a new conda
environment (e.g. `conda create -n cqgui`), activating it (`conda activate cqgui`), and running
`conda install -c cadquery -c conda-forge cq-editor=master`. But this didn't work for me.

#### Installer Script

The easiest way to install the GUI is to download and extract the [zipped
release](https://github.com/CadQuery/CQ-editor/releases) for your system. It contains a script
called `CQ-editor.sh`, and an executable `CQ-editor/CQ-editor`. The script just calls the executable
after setting an environment variable and running `chmod +x` on the executable. On Mac the
environment variable may matter; on Linux it works fine to run the executable directly.

However, I found that this release was out of date compared to what I got with a development install
of the standalone library. This was frustrating because some commands that worked in the standalone
version wouldn't work in the GUI. (As of February 2022, this included the cylinder command.) So I
wouldn't recommend using this method right now. In the future, when CADQuery is more stable, this
may become the best bet.
