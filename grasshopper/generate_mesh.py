import json
import pathlib
import rhino3dm
import compute_rhino3d.Util
import compute_rhino3d.Grasshopper as gh

compute_rhino3d.Util.url = "http://localhost:8081/"
#compute_rhino3d.Util.apiKey = ""

def generate_default_parameters(outdir = "."):
    this_dir = pathlib.Path(outdir).resolve()
    path_stl = this_dir / "vortex_generators.stl"
    path_3dm = this_dir / "vortex_generators.3dm"
    parameters = {
        "platform_length" : 8.0,
        "step_length" : 3.0,
        "step_height" : 0.5,
        "floor_thickness" : 1.5,
        "model_width" : 12.0,
        "vg_length" : 0.5,
        "vg_height" : 0.2,
        "vg_thickness" : 0.05,
        "vg_angle" : 0.2,
        "vg_inter_spacing" : 0.6,
        "vg_intra_spacing" : 1.2,
        "n_vg_pairs" : 6,
        "stl_path" : path_stl.as_posix(),
        "save_stl" : True,
        "3dm_path" : path_3dm.as_posix(),
        "save_3dm" : True
    }
    return parameters

# Create input trees.
def generate_3dm_and_stl(parameters):
    trees = []
    def add_tree(name):
        tree = gh.DataTree(name)
        tree.Append([0], [parameters[name]])
        trees.append(tree)

    add_tree("platform_length")
    add_tree("step_length")
    add_tree("step_height")
    add_tree("floor_thickness")
    add_tree("model_width")
    add_tree("vg_length")
    add_tree("vg_height")
    add_tree("vg_thickness")
    add_tree("vg_angle")
    add_tree("vg_inter_spacing")
    add_tree("vg_intra_spacing")
    add_tree("n_vg_pairs")
    add_tree("stl_path")
    add_tree("save_stl")

    # Evaluate the model.
    path_stl = parameters["stl_path"]
    print(f"Writing mesh to {path_stl}")
    output = gh.EvaluateDefinition('vortex_generators.gh', trees)

    # Decode results.
    branch = output['values'][0]['InnerTree']['{0}']
    breps = [rhino3dm.CommonObject.Decode(json.loads(item['data'])) for item in branch]

    # Save a 3dm file, if requested.
    if parameters["save_3dm"]:
        path_3dm = parameters["3dm_path"]
        print(f"Writing {len(breps)} breps to {path_3dm}")
        model = rhino3dm.File3dm()
        for brep in breps:
            model.Objects.AddBrep(brep)
        model.Write(path_3dm)

if __name__ == "__main__":
    parameters = generate_default_parameters()
    generate_3dm_and_stl(parameters)

