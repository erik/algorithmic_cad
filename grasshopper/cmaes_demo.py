import cma
import pathlib
from generate_mesh import generate_default_parameters, generate_3dm_and_stl

def optimize(outdir = "."):
    outdir = pathlib.Path(outdir)
    outdir.mkdir(parents=True, exist_ok=True)
    outdir = outdir.resolve()
    logfile = open(outdir / "optimization_log.txt", "w")

    # We will "optimize" the height and angle.
    # We'll start with a distribution centered at a height of 0.1 and an angle of 0.2 rad.
    # The second parameter sets sigma.
    es = cma.CMAEvolutionStrategy([0.1, 0.2], 0.2)
    gen = 1
    parameters = generate_default_parameters()
    while not es.stop():
        # Ask CMA-ES for the new generation of potential solutions.
        solutions = es.ask()

        # Write them to our log.
        for idx, solution in enumerate(solutions):
            logfile.write(f"gen {gen} sample {idx + 1}: ")
            logfile.write(' '.join([str(x) for x in solution]) + '\n')

        # Generate and save the meshes.
        for idx, solution in enumerate(solutions):
            parameters["vg_height"] = solutions[idx][0]
            parameters["vg_angle"] = solutions[idx][1]
            path_stl = outdir / f"gen_{gen}_s_{idx + 1}_vg.stl"
            path_3dm = outdir / f"gen_{gen}_s_{idx + 1}_vg.3dm"
            parameters["stl_path"] = path_stl.as_posix() 
            parameters["3dm_path"] = path_3dm.as_posix() 
            generate_3dm_and_stl(parameters)
        
        # Score each sample.
        # Here is where we will launch the simulations.
        # For now I'm just going to cheat and define a simple objective.
        scores = []
        for idx, solution in enumerate(solutions):
            height = solutions[idx][0]
            angle = solutions[idx][1]
            score = (height - 0.25)**2 + (angle - 0.3)**2
            scores.append(score)
        
        # Log results and submit them to CMA-ES.
        logfile.write(f"gen {gen} scores: ")
        logfile.write(' '.join([str(x) for x in scores]) + '\n')
        es.tell(solutions, scores)

        # This occasionally prints a one line summary of the current state.
        es.disp()
        gen += 1

    # Save the final result.
    result = es.result_pretty()
    solution = result.xbest
    logfile.write("best parameters: ")
    logfile.write(' '.join([str(x) for x in solution]) + '\n')


if __name__ == "__main__":
    optimize("data")

