# Rhino/Grasshopper

![Rhino/Grasshopper](img/grasshopper.jpg)

## Quick Links

- [Rhino downloads](https://www.rhino3d.com/download/)
- [Grasshopper docs](https://grasshopperdocs.com/)
- [Rhino3dm repo](https://github.com/mcneel/rhino3dm.git)
- [Rhino Compute Server repo](https://github.com/mcneel/compute.rhino3d)

## About

[Rhino](https://www.rhino3d.com/) is a CAD program that's particularly popular in the
architecture world. [Grasshopper](https://www.grasshopper3d.com/) is a dataflow
algorithmic design plugin for Rhino. It was originally an extension that you'd have to
download and install separately, but it became wildly
popular and is now packaged with Rhino.

On its own, Grasshopper is already a powerful tool for parametric and algorithmic design.
It's been a mainstay of generative architecture for years. Traditionally, it's been used
from within Rhino, but it's becoming easier and easier to run Rhino/Grasshopper "headless",
i.e. from the command line and without a GUI. This is very helpful for full algorithmic
design, where you may want to batch process thousands of variations.

Since 2018, there has been a Python library called [rhino3dm](https://www.rhino3d.com/features/rhino-inside). This library allows you to read and write Rhino's `.3dm` CAD files.
It also lets you construct geometry, but you're somewhat limited in which operations you
can use. Closest point calculations, intersections, and tessallations, [for example](https://developer.rhino3d.com/guides/compute/features/) are off the table.

But new in Rhino 7 is something called [Rhino.Inside](https://www.rhino3d.com/features/rhino-inside/). Rhino.Inside allows Rhino
to be embedded in other applications as a geometry/CAD kernel. One particulary interesting use of Rhino.Inside is
[Rhino Compute Server](https://github.com/mcneel/compute.rhino3d). This is a standalone (headless) server that exposes
a [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) API for basically all of Rhino.
The server itself is a .NET application, and it comes with a Python library that lets us interact with the server.
This is what we will be exploring here.



## Installing

### Rhino/Grasshopper

Rhino 7 installers can be downloaded from [here](https://www.rhino3d.com/download/). Note that it's
not free software, so you have to have a license to use it. In the School of Architecture
and Planning here at MIT, we're lucky to have a department-wide license. Otherwise, McNeel
offers free 90 day trials if you give them your email addresss. Rhino 7 includes Grasshopper.

### Rhino Compute Server

To compile the server, you'll need to install [Visual Studio](https://visualstudio.microsoft.com/).
Instructions for this are [here](https://developer.rhino3d.com/guides/compute/development/).

You also need to install the Python library, which you can do using pip.

```
python -m pip install compute_rhino3d
```

### Rhino3dm

This one is just another pip call.

```
python -m pip install rhino3dm
```

### CMA-ES

To run the optimization demo, you'll need a few extra Python libraries installed.

```
python -m pip install cma numpy matplotlib
```
